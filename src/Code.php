<?php

namespace Autocarat\Core;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Code
 * @package Autocarat\Core
 *
 * @property int $id
 * @property string $name
 * @property string $name_str
 * @property int $name_int
 * @property string $name_sfx
 * @property string $note
 * @property \Autocarat\Core\Client $client
 * @property \Illuminate\Database\Eloquent\Collection $exClients
 */
class Code extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $appends = ['name'];
    protected $visible = ['id', 'name'];

    public function client()
    {
        return $this->hasOne(Client::class, 'code_id');
    }

    public function exClients()
    {
        return $this->hasMany(Client::class, 'the_last_code_id');
    }

    public function getNameAttribute()
    {
        return
            ($this->name_str ?: null) .
            (intval($this->name_int) ?: null) .
            (!empty($this->name_sfx)
                ?
                '-' . $this->name_sfx
                :
                null
            );
    }

    public function setNameAttribute($name)
    {
        $trio = self::splitName($name);
        $this->attributes['name_str'] = $trio['str'] ?: '';
        $this->attributes['name_int'] = $trio['int'] ?: 0;
        $this->attributes['name_sfx'] = $trio['sfx'] ?: '';
    }

    protected static function splitName($name)
    {
        $chars = array_values(
            array_filter(
                preg_split('//u', mb_strtoupper($name), -1, PREG_SPLIT_NO_EMPTY),
                function ($char) {
                    return false !== mb_strpos('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', $char);
                }
            )
        );

        $str = '';
        $int = '';
        $sfx = '';

        for ($s = 0; $s < count($chars); ++$s) {
            if (is_numeric($chars[$s])) {
                //
                for ($i = $s; $i < count($chars); ++$i) {
                    if (is_numeric($chars[$i])) {
                        $int .= $chars[$i];
                    } else {
                        //
                        for ($f = $i; $f < count($chars); ++$f) {
                            $sfx .= $chars[$f];
                        }
                        break;
                    }
                }
                break;
            }
            $str .= $chars[$s];
        }

        return [
            'str' => $str,
            'int' => intval($int),
            'sfx' => $sfx
        ];
    }
}
