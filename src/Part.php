<?php

namespace Autocarat\Core;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Part extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'part_number',
        'cent_price',
        'rg_id',
        'warning',
        'cent_pfand',
        'gram_weight',
        'teileart'
    ];

    public function rg()
    {
        return $this->belongsTo(RG::class);
    }

    public static function fixNumber(string $number): string
    {
        // [old] = [new] (replacement)
        $replacement = preg_split('/=/u', $number);
        $number = end($replacement);

        $number = mb_strtoupper(
            trim($number)
        );

        $number = str_replace(
            ['А', 'В', 'С', 'Е', 'І', 'К', 'М', 'Н', 'О', 'Р', 'Т', 'Х', 'У'],  // cyrillic
            ['A', 'B', 'C', 'E', 'I', 'K', 'M', 'H', 'O', 'P', 'T', 'X', 'Y'],  // latin
            $number
        );

        // only allowed symbols
        return implode(
            array_filter(
                preg_split('//u', $number, -1, PREG_SPLIT_NO_EMPTY),
                function ($char) {
                    return false !== mb_strpos('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', $char);
                })
        );

        /*
         * preg_split vs mb_split
         * https://stackoverflow.com/questions/36115796/preg-split-vs-mb-split
         */
    }
}
