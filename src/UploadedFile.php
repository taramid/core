<?php

namespace Autocarat\Core;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UploadedFile
 * @package App
 *
 * @property int $id
 * @property int $upload_id
 * @property string $name
 * @property string $path
 */
class UploadedFile extends Model
{
    protected $fillable = [
        'upload_id',
        'name',
        'path'
    ];

    public function upload()
    {
        return $this->belongsTo(Upload::class);
    }
}
