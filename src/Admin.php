<?php

namespace Autocarat\Core;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Admin
 * @package Autocarat\Core
 *
 * @property int $id
 * @property string $name
 * @property int $role
 * @property string $note
 * @property string $email
 * @property \Illuminate\Database\Eloquent\Collection $supplies
 */
class Admin extends Authenticatable
{
    const ROLE_ADMIN = 8;
    const ROLE_MANAGER = 19;

    use Notifiable;
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'note', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
        return Admin::ROLE_ADMIN === $this->role;
    }

    public function supplies()
    {
        return $this->belongsToMany(Supply::class)
            ->orderBy('name', 'asc')
            ->withTimestamps();
    }
}
