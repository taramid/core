<?php

namespace Autocarat\Core;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Loyalty
 * @package Autocarat\Core
 *
 * @property int $id
 * @property int $supply_id
 * @property string $name
 * @property \Autocarat\Core\Supply $supply
 * @property \Illuminate\Database\Eloquent\Collection $discounts
 * @property \Illuminate\Database\Eloquent\Collection $client
 */
class Loyalty extends Model
{
    protected $fillable = ['supply_id', 'name'];

    public function supply()
    {
        return $this->belongsTo(Supply::class);
    }

    public function discounts()
    {
        return $this->hasMany(Discount::class);
    }

    public function clients()
    {
        return $this->belongsToMany(
            Client::class,
            'client_loyalty',
            'loyalty_id',
            'client_id'
        )
            ->withTimestamps();
    }
}
