<?php

namespace Autocarat\Core;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'note'
    ];

    public function supplies()
    {
        return $this->hasMany(Supply::class);
    }
}
