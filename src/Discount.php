<?php

namespace Autocarat\Core;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Discount
 * @package Autocarat\Core
 *
 * @property int $id
 * @property int $rg_id
 * @property int $loyalty_id
 * @property float $percent
 * @property \Autocarat\Core\RG $rg
 * @property \Autocarat\Core\Loyalty $loyalty
 */
class Discount extends Model
{
    protected $fillable = ['rg_id', 'loyalty_id', 'percent'];

    public function rg()
    {
        return $this->belongsTo(RG::class);
    }

    public function loyalty()
    {
        return $this->belongsTo(Loyalty::class);
    }
}
