<?php

namespace Autocarat\Core;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RG
 * @package Autocarat\Core
 *
 * @property int $id
 * @property int $supply_id
 * @property string $name
 * @property float $percent
 * @property \Autocarat\Core\Supply $supply
 * @property \Illuminate\Database\Eloquent\Collection $parts
 */
class RG extends Model
{
    protected $table = 'rgs';

    protected $fillable = [
        'supply_id', 'name'
    ];

    public function supply()
    {
        return $this->belongsTo(Supply::class);
    }

    public function parts()
    {
        return $this->hasMany(Part::class);
    }
}
