<?php

namespace Autocarat\Core;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Client
 * @package Autocarat\Core
 *
 * @property int $id
 * @property int $code_id
 * @property string $code_history
 * @property string $note
 * @property \Autocarat\Core\Code $code
 * @property \Illuminate\Database\Eloquent\Collection $users
 */
class Client extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $bIsBeingDeleted = false;

    public function code()
    {
        return $this->belongsTo(Code::class);
    }

    public function users()
    {
        return
            $this->belongsToMany(User::class)
                ->withPivot('can_order')
                ->withTimestamps();
    }

    public function loyalties()
    {
        return $this->belongsToMany(
            Loyalty::class,
            'client_loyalty',
            'client_id',
            'loyalty_id'
        )
            ->withTimestamps();
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    protected static function boot()
    {
        parent::boot();

        static::updating(function ($client) {
            //
            $originalCode = Code::find($client->getOriginal('code_id'));
            if (!$client->bIsBeingDeleted && isset($originalCode) && $client->code_id != $originalCode->id) {
                //
                $client->code_history = trim(
                    $client->code_history . ',' . $originalCode->name,
                    ','
                );
            }
        });

        static::deleting(function ($client) {
            //
            $code = Code::find($client->code_id);
            if (isset($code)) {
                //
                $client->code_history = trim(
                    $client->code_history . ',' . $code->name,
                    ','
                );
            }

            $client->code_id = null;

            $client->bIsBeingDeleted = true;
            $client->timestamps = false;
            $client->save();
        });
    }
}
