<?php

namespace Autocarat\Core;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Upload
 * @package App
 *
 * @property int $id
 * @property int $uploadable
 * @property Admin $admin
 * @property array $files
 */
class Upload extends Model
{
    public function uploadable()
    {
        return $this->morphTo();
    }

    public function files()
    {
        return $this->hasMany(UploadedFile::class);
    }
}
