<?php

namespace Autocarat\Core;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderItem extends Model
{
    use SoftDeletes;

    protected $casts = [
        'processed_at' => 'datetime:U',
        'created_at' => 'datetime:U',
        'updated_at' => 'datetime:U',
        'deleted_at' => 'datetime:U',
    ];

    protected $dates = [
        'processed_at',
        'deleted_at',
    ];

    protected $fillable = [
        'order_id', 'part_id', 'amount', 'title', 'note', 'state'
    ];

    protected $touches = [
        'order'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function parcels()
    {
        return $this->hasMany(Parcel::class);
    }
}
