<?php

namespace Autocarat\Core;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Supply
 * @package Autocarat\Core
 *
 * @property int $id
 * @property int $manufacturer_id
 * @property int $supplier_id
 * @property string $name
 * @property string $alias
 * @property string $note
 * @property bool $is_in_maintenance
 * @property \Illuminate\Database\Eloquent\Collection $admins
 * @property \Autocarat\Core\Manufacturer $manufacturer
 * @property \Autocarat\Core\Supplier $supplier
 * @property \Illuminate\Database\Eloquent\Collection $rgs
 * @property \Illuminate\Database\Eloquent\Collection $parts
 * @property \Illuminate\Database\Eloquent\Collection $loyalties
 */
class Supply extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'manufacturer_id', 'supplier_id', 'name', 'alias', 'note', 'is_in_maintenance',
    ];

    public function admins()
    {
        return $this->belongsToMany(Admin::class)->withTimestamps();
    }

    public function manufacturer()
    {
        return $this->belongsTo(Manufacturer::class);
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function rgs()
    {
        return $this->hasMany(RG::class)->orderBy('name');
    }

    public function parts()
    {
        return $this->hasManyThrough(Part::class, RG::class);
    }

    public function loyalties()
    {
        return $this->hasMany(Loyalty::class)->orderBy('id');
    }
}
