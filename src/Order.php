<?php

namespace Autocarat\Core;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $casts = [
        'created_at' => 'datetime:U',
        'updated_at' => 'datetime:U',
        'deleted_at' => 'datetime:U',
    ];

    protected $dates = [
        'deleted_at',
    ];

    protected $fillable = [
        'client_id', 'admin_id', 'note'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
