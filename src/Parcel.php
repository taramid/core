<?php

namespace Autocarat\Core;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Parcel extends Model
{
    use SoftDeletes;

    protected $casts = [
        'given_at' => 'datetime:U',
        'dispatched_at' => 'datetime:U',
        'created_at' => 'datetime:U',
        'updated_at' => 'datetime:U',
        'deleted_at' => 'datetime:U',
    ];

    protected $dates = [
        'given_at',
        'dispatched_at',
        'deleted_at',
    ];

    protected $fillable = [
        'order_item_id', 'part_number'
    ];

    public function minion()
    {
        return $this->belongsTo(Minion::class);
    }

    public function orderItem()
    {
        return $this->belongsTo(OrderItem::class);
    }
}
