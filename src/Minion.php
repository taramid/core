<?php

namespace Autocarat\Core;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Minion extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'code', 'note'
    ];

    public function parcels()
    {
        return $this->hasMany(Parcel::class);
    }
}
