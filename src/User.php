<?php

namespace Autocarat\Core;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * @package Autocarat\Core
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $note
 * @property string $phone
 * @property \Illuminate\Database\Eloquent\Collection $clients
 */
class User extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function clients()
    {
        return
            $this->belongsToMany(Client::class)
                ->withPivot('can_order')
                ->withTimestamps();
    }
}
