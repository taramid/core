<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discounts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('rg_id');
            $table->unsignedInteger('loyalty_id');
            $table->float('percent', 8, 4)->nullable();
            $table->timestamps();

            $table->unique(['rg_id', 'loyalty_id']);

            $table->foreign('rg_id')
                ->references('id')->on('rgs')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('loyalty_id')
                ->references('id')->on('loyalties')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discounts');
    }
}
