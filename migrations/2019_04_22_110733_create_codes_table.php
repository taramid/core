<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('codes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_str')->default('');
            $table->unsignedInteger('name_int')->default(0);
            $table->string('name_sfx')->default('');
            $table->string('note')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['name_str', 'name_int', 'name_sfx']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('codes');
    }
}
