<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParcelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parcels', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('minion_id');
            $table->unsignedInteger('order_item_id')->nullable();
            $table->string('part_number');
            $table->timestamp('given_at')->nullable();
            $table->timestamp('dispatched_at')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('minion_id')
                ->references('id')->on('minions')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('order_item_id')
                ->references('id')->on('order_items')
                ->onDelete('set null')
                ->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parcels');
    }
}
