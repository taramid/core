<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplies', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('manufacturer_id');
            $table->unsignedInteger('supplier_id');
            $table->string('name');
            $table->string('alias')->comment('as seen by client');
            $table->string('note')->nullable();
            $table->boolean('is_in_maintenance')->default(true);
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['manufacturer_id', 'supplier_id', 'name']);

            $table->foreign('manufacturer_id')
                ->references('id')->on('manufacturers')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('supplier_id')
                ->references('id')->on('suppliers')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplies');
    }
}
