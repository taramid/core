<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientLoyaltyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_loyalty', function (Blueprint $table) {
            $table->unsignedInteger('client_id');
            $table->unsignedInteger('loyalty_id');
            $table->timestamps();

            $table->primary(['client_id', 'loyalty_id']);

            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->foreign('loyalty_id')->references('id')->on('loyalties')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_loyalty');
    }
}
