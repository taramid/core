<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRGsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rgs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('supply_id');
            $table->string('name', 6);
            $table->float('percent', 8, 4)->nullable()->comment('incoming discount');
            $table->timestamps();

            $table->unique(['supply_id', 'name']);

            $table->foreign('supply_id')
                ->references('id')->on('supplies')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rgs');
    }
}
