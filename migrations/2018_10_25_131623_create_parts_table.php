<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('part_number', 42);
            $table->integer('cent_price');
            $table->unsignedInteger('rg_id');
            $table->string('warning')->nullable();
            $table->integer('cent_pfand')->nullable();
            $table->integer('weight')->nullable()->comment('in gram');
            $table->tinyInteger('teileart')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['part_number', 'rg_id']);

            $table->foreign('rg_id')
                ->references('id')->on('rgs')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parts');
    }
}
